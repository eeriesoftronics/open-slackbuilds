# OpenSlackbuilds

OpenSlackbuilds is intended as a parallel to SlackBuilds.org.

# Integration with VSlack Package Manager

OpenSlackbuilds integrates easily with [VSlack Package Manager](http://bitbucket.org/vredens/vslack-package-manager). If you already have a VSlack Package Manager local repository sync'ed with SlackBuilds.org you can easily update it with the OpenSlackbuilds by typing:

	vs-pm repo add open-slackbuilds https://bitbucket.org/eeriesoftronics/open-slackbuilds.git all
	vs-pm repo update open-slackbuilds
	vs-pm rescan

You need to run the commands as the root user or using `sudo` to gain root permissions.
